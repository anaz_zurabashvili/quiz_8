package com.example.quiz_8.common

object ApiEndpoints {
    const val BASE_URL = "https://run.mocky.io/v3/"
    const val ID = "f39fdeed-3069-4bf0-b80c-f70199b7a975"
}