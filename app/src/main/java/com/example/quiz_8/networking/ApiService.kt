package com.example.quiz_8.networking


import com.example.quiz_8.common.ApiEndpoints
import com.example.quiz_8.models.CourseData
import retrofit2.Response
import retrofit2.http.GET


interface ApiService {

    @GET(ApiEndpoints.ID)
    suspend fun getCourses(): Response<CourseData>


}


