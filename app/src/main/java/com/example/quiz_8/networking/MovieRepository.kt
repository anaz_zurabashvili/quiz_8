package com.example.quiz_8.networking

import com.example.quiz_8.common.Resource
import com.example.quiz_8.models.CourseData
import javax.inject.Inject

class CourseRepository @Inject constructor(
    private val api: ApiService
) {
    suspend fun getCourses(): Resource<CourseData> {
        return try {
            Resource.Loading(null)
            val response = api.getCourses()
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(data = null, response.message().toString())
            }
        } catch (e: Exception) {
            Resource.Error(data = null, e.toString())
        }
    }

}