package com.example.quiz_8.models


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ActiveCourse(
    @Json(name = "background_color_present")
    val backgroundColorPresent: String?,
    @Json(name = "booking_time")
    val bookingTime: String?,
    val id: String?,
    val image: String?,
    @Json(name = "main_color")
    val mainColor: String?,
    @Json(name = "play_button_color_present")
    val playButtonColorPresent: String?,
    val progress: String?,
    val title: String?
)