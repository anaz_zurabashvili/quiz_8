package com.example.quiz_8.models


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewCourse(
    val duration: String?,
    @Json(name = "icon_type")
    val iconType: String?,
    val id: String?,
    @Json(name = "main_color")
    val mainColor: String?,
    val question: String?,
    val title: String?
)