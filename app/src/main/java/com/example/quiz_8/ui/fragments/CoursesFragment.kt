package com.example.quiz_8.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz_8.databinding.FragmentCoursesBinding
import com.example.quiz_8.ui.CoursesViewModel
import com.example.quiz_8.ui.adapters.ActiveCourseAdapter
import com.example.quiz_8.ui.adapters.NewCourseAdapter
import com.example.quiz_8.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CoursesFragment  : BaseFragment<FragmentCoursesBinding>(FragmentCoursesBinding::inflate) {
    private val viewModel: CoursesViewModel by viewModels()
    private lateinit var activeCourseAdapter: ActiveCourseAdapter
    private lateinit var newCourseAdapter: NewCourseAdapter
    override fun start() {
        initRVNews()
        initRVActive()
        setObservers()
    }

    private fun initRVNews() {
        binding.rvNewCourses.apply {
            layoutManager = LinearLayoutManager(view?.context)
            newCourseAdapter = NewCourseAdapter()
            adapter = newCourseAdapter

        }
    }
    private fun initRVActive() {
        binding.rvActiveCourses.apply {
            layoutManager = LinearLayoutManager(view?.context)
            activeCourseAdapter = ActiveCourseAdapter()
            adapter = activeCourseAdapter
        }
    }

    private fun setObserversActive() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.coursesActiveFlow() { it ->
                activeCourseAdapter.submitData(it)
            }
        }
    }

    private fun setObserversNews() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.coursesActiveFlow() { it ->
                activeCourseAdapter.submitData(it)
            }
        }
    }
}