package com.example.quiz_8.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.example.quiz_8.networking.CourseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class CoursesViewModel @Inject constructor(
    private val repository: CourseRepository
) : ViewModel() {

    suspend fun coursesNewFlow() = repository.getCourses().data!!.newCourses
    suspend fun coursesActiveFlow() = repository.getCourses().data!!.activeCourses

}