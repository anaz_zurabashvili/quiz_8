package com.example.quiz_8.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_8.databinding.ItemActiveCourseBinding
import com.example.quiz_8.models.ActiveCourse

class ActiveCourseAdapter: RecyclerView<ActiveCourse, ActiveCourseAdapter.ViewHolder>(DiffCallback())  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            ItemUsersBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: UsersAdapter.ViewHolder,
        position: Int
    ) =
        holder.onBind(getItem(position)!!)

    inner class ViewHolder(private val binding: ItemActiveCourseBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(model : User.Data) {
            binding.tvId.text = model.id.toString()
            binding.tvFirstName.text = model.firstName.plus(" ${model.lastName}")
            binding.imAvatar.setImageUrl(model.avatar)

        }
    }

    class DiffCallback : DiffUtil.ItemCallback<User.Data>(){
        override fun areItemsTheSame(oldItem: User.Data, newItem: User.Data): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: User.Data, newItem: User.Data): Boolean =
            oldItem == newItem
    }
}